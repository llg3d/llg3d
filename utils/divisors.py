"""Calculates the list of divisors of an integer"""
import argparse


def get_divisors(n: int) -> list:
    """Returns the list of divisors of n"""
    divisors = []
    i = 1
    while i <= n**0.5:
        if n % i == 0:
            divisors.append(i)
            divisors.append(n // i)
        i += 1
    divisors.sort()
    return divisors


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('n', type=int, help='Integer')
    args = parser.parse_args()

    print(' '.join(str(n) for n in get_divisors(args.n)))