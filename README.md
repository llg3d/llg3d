# LLG3D: A solver for the stochastic Landau-Lifshitz-Gilbert equation in 3D

[![pipeline status](https://gitlab.math.unistra.fr/llg3d/llg3d/badges/main/pipeline.svg)](https://gitlab.math.unistra.fr/llg3d/llg3d/-/commits/main)
[![coverage report](https://gitlab.math.unistra.fr/llg3d/llg3d/badges/main/coverage.svg)](https://llg3d.pages.math.unistra.fr/llg3d/coverage)
[![Latest Release](https://gitlab.math.unistra.fr/llg3d/llg3d/-/badges/release.svg)](https://gitlab.math.unistra.fr/llg3d/llg3d/-/releases)
[![Doc](https://img.shields.io/badge/doc-sphinx-blue)](https://llg3d.pages.math.unistra.fr/llg3d/)
[![SWH](https://archive.softwareheritage.org/badge/origin/https://gitlab.math.unistra.fr/llg3d/llg3d/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.math.unistra.fr/llg3d/llg3d)

LLG3D is written in Python and utilizes the MPI library for parallelizing computations.

See the [documentation](https://llg3d.pages.math.unistra.fr/llg3d/).
