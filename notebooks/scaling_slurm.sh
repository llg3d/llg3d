#!/bin/bash

set -euo pipefail

mkdir -p run
cd run/ || exit

OUTFILE=scaling_gaya.csv
echo "Output will be written to ${OUTFILE}"

echo "np,ite_time" >"${OUTFILE}"

for n in 1 2 4 12 20 25 40 50 100 120; do
	slurmfile=sbatch_"${n}".slurm
	nite=$((100 * n))
	cmd="mpiexec -n ${n} python ../../llg3d.py -N ${nite} -Jx 6000 -dx 1e-9 | grep -i \"temps/ite\" | cut -d '=' -f2"
	echo "${cmd}"
	cat >"${slurmfile}" <<EOL
#!/bin/bash

#SBATCH -p public
#SBATCH --ntasks-per-core=1
#SBATCH -N 1
#SBATCH -n ${n}
#SBATCH -J g_${n}
#SBATCH --exclusive

source .venv/bin/activate

echo "${cmd}"
ite_time=\`${cmd}\`
echo "ite_time = \${ite_time}"
echo "${n},\${ite_time}" >> "${OUTFILE}"
EOL
	sbatch "${slurmfile}"
done
