#!/bin/bash

set -euo pipefail

TMPDIR=/tmp
OUTFILE=${1:?Usage: $0 <outfile.csv>}

echo "np, ite_time" > "${OUTFILE}"

for n in 1 2 3 4 5 6 8 10 12 15; do
    nite=$((100 * n))
    echo "np = $n nite = $nite"
    ite_time=$(mpiexec -n "$n" llg3d -N "$nite" -Jx 600 -dx 1e-9 | grep "time/ite" | cut -d '=' -f2)
    echo "ite_time = $ite_time"
    echo "${n},${ite_time}" >> "${OUTFILE}"
done

echo "Output written to ${OUTFILE}:"
cat "${OUTFILE}"