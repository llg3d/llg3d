#!/usr/bin/env python3
"""Plots acceleration as a function of the number of MPI processes."""

from matplotlib import pyplot as plt
import numpy as np
import argparse
import re


def plot(filename: str):
    """Read data from csv file and plot it. Export to PNG."""
    data = np.genfromtxt(filename, delimiter=",", skip_header=1)
    data = data[data[:, 0].argsort()]  # Sort by increasing nprocs
    nprocs, exectime = data[:, 0], data[:, 1]

    acceleration = exectime[0] / exectime[:]

    fig = plt.figure()
    fig.suptitle("Acceleration as a function of the number of MPI processes")
    plt.plot(nprocs, acceleration, "o", label="measured")
    plt.plot(nprocs, nprocs, label="ideal")
    plt.xlabel("Number of processes")
    plt.ylabel("Acceleration")

    # Get Jx, Jy, and Jz values from filename
    m = re.match(r".*_(\d+)x(\d+)x(\d+).*\.csv", filename)
    if m:
        Jx, Jy, Jz = m.groups()
        plt.title(f"Grid {Jx} x {Jy} x {Jz}")

    plt.legend()
    plt.show()

    imagefilename = filename.replace(".csv", ".png")
    fig.savefig(imagefilename)
    print(f"Image saved to {imagefilename}")


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("filename", help="Data file")
    args = parser.parse_args()
    plot(args.filename)


if __name__ == "__main__":
    main()
