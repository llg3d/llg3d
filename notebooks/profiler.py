"""Profile using cProfile and dump one file per process."""
import cProfile
from mpi4py.MPI import COMM_WORLD as comm
from llg3d import llg3d

pr = cProfile.Profile()
pr.enable()
llg3d.main()
pr.disable()
pr.dump_stats(f'cpu_{comm.rank}.prof')
