FROM ubuntu:22.04

RUN apt-get update && \
    apt-get install -y libopenmpi-dev openmpi-bin pandoc python3 python3-pip git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Switch to user llg3d
RUN useradd -ms /bin/bash llg3d
USER llg3d
ENV PATH="${PATH}:/home/llg3d/.local/bin"
WORKDIR /home/llg3d

# Copy the pyproject.toml to home directory
COPY --chown=llg3d:llg3d pyproject.toml .

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir toml-to-requirements && \
    toml-to-req --toml-file pyproject.toml --optional-lists test,doc && \
    pip install --no-cache-dir -r requirements.txt