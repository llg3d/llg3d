# Developer's Corner

## Running the Unoptimized Version

Switch to the `initial` branch and execute the script with the same name.

```bash
git checkout initial
python llg3d.py
```

## Running the Purely Sequential Version

```bash
python -m llg3d.llg3d_seq -N 100
```

It should yield the same results as the parallel version without MPI invocation:

```bash
llg3d -N 100
```

or:

```bash
mpirun -np 1 llg3d -N 100
```

## Profiling

Launch with cProfile:

```bash
mpirun -np 4 python -m cProfile -o llg3d_np4.prof -m llg3d.llg3d -N 5000
```

Visualize with [snakeviz](https://jiffyclub.github.io/snakeviz/):

```bash
snakeviz llg3d_np4.prof
```

## Unit Tests

### Install Test Dependencies

```bash
pip install -e ".[test]"
```

### Run Tests

Sequentially:

```bash
pytest
```

In parallel:

```bash
mpirun -np 6 python -m pytest -svv --with-mpi
```

## Building Documentation

### Install Sphinx Dependencies

```bash
pip install -e ".[doc]"
```

### Install pandoc

```bash
sudo apt-get install pandoc  # Ubuntu
brew install pandoc  # macOS
```

### Generate Documentation in Watch Mode

```bash
sphinx-autobuild --watch src/ docs/source docs/build/html
```

The documentation is locally accessible at <http://127.0.0.1:8000>.