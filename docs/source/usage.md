# Execute

## Command Line Help

```{command-output} llg3d -h

```

## Example

### Sequential Execution

```{command-output} llg3d -N 100

```


The execution produces the file `run.json` which contains the parameters and a link to the result file:

```{command-output} cat run.json
  :shell:

```

### Parallel Execution

```{command-output} mpirun -np 6 llg3d -N 100 ; rm -f *.npy *.json *.txt
  :shell:

```

```{note}
If the number of MPI processes `np` is not a divisor of `Jx`, the execution is interrupted.

```

## Parallel execution with SLURM on a computing cluster

### Install llg3d on the Cluster

```bash
# Create a working directory:
mkdir work
cd work
# Clone the llg3d sources:
git clone git@gitlab.math.unistra.fr:llg3d/llg3d.git
# Create and activate a Python virtual environment:
virtualenv .venv
source .venv/bin/activate
# Install llg3d (in editable mode):
pip install -e llg3d
# Create a run directory:
mkdir run
cd run
```

### Create a sbatch File

Copy the [`sbatch_jobarrays.slurm`](../../utils/sbatch_jobarrays.slurm) file into the `run/` directory:

```bash
cp ../llg3d/utils/sbatch_jobarrays.slurm .  # from the run/ directory
```

Its content is as follows:

```{literalinclude} ../../utils/sbatch_jobarrays.slurm
:language: bash
```

### Submit the Job Array

```bash
(.venv) $ sbatch sbatch_jobarrays.slurm
Submitted batch job 3672
```

The execution will create a [SLURM job array](https://slurm.schedmd.com/job_array.html) where each sub-job corresponds to a temperature.

### Monitor Job Execution

```bash
(.venv) $ squeue
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
            3672_0    public      g40  boileau  R       0:02      1 gaya1
            3672_1    public      g40  boileau  R       0:02      1 gaya1
            3672_2    public      g40  boileau  R       0:02      1 gaya1
            3672_3    public      g40  boileau  R       0:02      1 gaya2
            3672_4    public      g40  boileau  R       0:02      1 gaya2
            3672_5    public      g40  boileau  R       0:02      1 gaya2
            3672_6    public      g40  boileau  R       0:02      1 gaya3
            3672_7    public      g40  boileau  R       0:02      1 gaya3
            3672_8    public      g40  boileau  R       0:02      1 gaya3
            3672_9    public      g40  boileau  R       0:02      1 gaya4
           3672_10    public      g40  boileau  R       0:02      1 gaya4
           3672_11    public      g40  boileau  R       0:02      1 gaya4
           3672_12    public      g40  boileau  R       0:02      1 gaya5
```

It can be seen that jobs `[0-12]` have already started (`R` for _running_).

When the jobs are finished, they leave the queue:

```bash
(.venv) $ squeue
             JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
```

The execution produces the following directory structure:

```bash
(.venv) $ tree
.
├── job_3672
│   ├── 000
│   │   ├── m1_integral_space_T1000_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 001
│   │   ├── m1_integral_space_T1100_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 002
│   │   ├── m1_integral_space_T1200_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 003
│   │   ├── m1_integral_space_T1300_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 004
│   │   ├── m1_integral_space_T1350_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 005
│   │   ├── m1_integral_space_T1400_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 006
│   │   ├── m1_integral_space_T1425_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 007
│   │   ├── m1_integral_space_T1450_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 008
│   │   ├── m1_integral_space_T1500_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 009
│   │   ├── m1_integral_space_T1550_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 010
│   │   ├── m1_integral_space_T1650_6000x21x21_np40.txt
│   │   └── run.json
│   ├── 011
│   │   ├── m1_integral_space_T1750_6000x21x21_np40.txt
│   │   └── run.json
│   └── 012
│       ├── m1_integral_space_T1900_6000x21x21_np40.txt
│       └── run.json
├── sbatch_jobarrays.slurm
├── slurm-3672_0.out
├── slurm-3672_10.out
├── slurm-3672_11.out
├── slurm-3672_12.out
├── slurm-3672_1.out
├── slurm-3672_2.out
├── slurm-3672_3.out
├── slurm-3672_4.out
├── slurm-3672_5.out
├── slurm-3672_6.out
├── slurm-3672_7.out
├── slurm-3672_8.out
└── slurm-3672_9.out

14 directories, 54 files
```

### Process the Results

Use the `llg3d.post` command which executes the `src/llg3d/process_temperature.py` script:

```bash
(.venv) $ llg3d.post job_451/
T_Curie = 1631 K
Image saved in job_451/m1_mean.png
```

`process_temperature.py` gathers the results, interpolates the values, and evaluates the Curie temperature as the value where the slope of the average magnetization is minimal: $T_{Curie} = \arg \min(\partial m_1/\partial T)$.

The plotted graph looks like this:

![Graph m = f(T)](m1_mean.png)