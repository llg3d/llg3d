# Installing llg3d

## Install MPI

On MacOS:

```bash
brew install open-mpi
```

## Install llg3d package

```bash
pip install llg3d
```

## Install llg3d in editable mode

### Clone the repository

```bash
git clone git@gitlab.math.unistra.fr:llg3d/llg3d.git
cd llg3d/
```

### Install with pip

```bash
pip install -e .
```