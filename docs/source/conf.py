# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information
from llg3d import __version__

project = "llg3d"
copyright = "2023, IRMA"
author = "IRMA"
release = __version__

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "myst_parser",
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.viewcode",
    "sphinx.ext.coverage",
    "sphinx.ext.graphviz",
    "sphinx.ext.autosummary",
    "sphinx_copybutton",
    "sphinx-prompt",
    "sphinx_last_updated_by_git",
    "nbsphinx",
    "sphinxcontrib.programoutput",
    'sphinxcontrib.bibtex',
]


templates_path = ["_templates"]
exclude_patterns = []


autodoc_default_options = {
    "members": True,
    "show-inheritance": True,
    "member-order": "groupwise",
    "private-members": False,
    "special-members": "__init__",
}

language = "en"

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "furo"
html_static_path = ["_static"]
html_css_files = ["styles/furo.css", "custom.css"]

myst_enable_extensions = [
    "dollarmath",
    "amsmath",
]

autosummary_generate = True  # Turn on sphinx.ext.autosummary

add_module_names = False

bibtex_bibfiles = ['refs.bib']
bibtex_default_style = 'unsrt'
