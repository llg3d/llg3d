# LLG3D

[![pipeline status](https://gitlab.math.unistra.fr/llg3d/llg3d/badges/main/pipeline.svg)](https://gitlab.math.unistra.fr/llg3d/llg3d/-/commits/main)
[![coverage report](https://gitlab.math.unistra.fr/llg3d/llg3d/badges/main/coverage.svg)](https://llg3d.pages.math.unistra.fr/llg3d/coverage)
[![Latest Release](https://gitlab.math.unistra.fr/llg3d/llg3d/-/badges/release.svg)](https://gitlab.math.unistra.fr/llg3d/llg3d/-/releases)
[![SWH](https://archive.softwareheritage.org/badge/origin/https://gitlab.math.unistra.fr/llg3d/llg3d/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.math.unistra.fr/llg3d/llg3d)

LLG3D is a solver for the stochastic Landau-Lifshitz-Gilbert equation in 3D.
It is written in Python and utilizes the MPI library to parallelize computations.
Details about the model, the numerical method and the physical results can be found in Courtès *et al.* {cite:p}`Courtes:2024aa`.


## Usage

```{toctree}
---
maxdepth: 2
caption: Utilisation
---
installation.md
usage.md
developers_corner.md
```

## Notebooks

```{toctree}
---
maxdepth: 1
caption: Notebooks
---
notebooks/acceleration.ipynb
notebooks/heat_1d.ipynb
notebooks/bench_laplacian.ipynb
```

## References

```{eval-rst}
.. autosummary::
   :toctree: _autosummary
   :caption: References
   :recursive:

   llg3d.llg3d
   llg3d.post
```

## Bibliography

```{bibliography}
:style: unsrt
```