# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.4.1] - 2024-06-25

### Fixed

- Add git to Docker image
- Fix issue with syntax highlighting in documentation

## [1.4.0] - 2024-06-25

### Changed

- Translate some code to English
- Update acceleration.ipynb
- Update AUTHORS with affiliations

## [1.3.0] - 2024-06-24

### Changed

- Translate the notebooks to English

### Added

- Add bibliography to the documentation
- `codemeta.json` for referencing in Software Heritage

### Fixed

- Fix the documentation for the post-processing

## [1.2.0] - 2024-06-18

### Added

- document pip install from pypi

## [1.1.0] - 2024-06-18

### Added

- CI: publish to pypi

## [1.0.0] - 2024-06-18

### Added

- export x-profiles of magnetization

### Changed

- initialization is now external functions
- Better CLI
- Translate the code and the documentation to English
- Reorganize post-processing

## [0.1.2] - 2023-11-10

### Added

- AUTHORS file

### Fixed

- typos in README.md

## [0.1.1] - 2023-11-08

### Added

- badges in README.md and sphinx

### Changed

- update documentation
- Better test coverage

## [0.1.0] - 2023-11-08

### Changed

- packaging with pyproject.toml instead of setup.cfg
- update scaling notebook

### Added

- documentation with sphinx
- setup CI with gitlab-ci
- this changelog
- MIT License
- Coverage report

## [0.0.1] - 2023-01-16

### Added

- First packaged version of llg3d
