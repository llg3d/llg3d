#!/usr/bin/env python3
"""
NOM : process_temperature_diff_dx.py
Post-traite un ensemble de runs regroupés en un fichier run.json ou
en un ensemble de job arrays SLURM :
  - Extrait les données de résultats,
  - Trace la magnétisation moyenne calculée en fonction de la température,
  - Interpole les points calculés sur des splines cubiques,
  - Détermine la température de Curie comme la valeur correspondant à
    la pente (négative) minimale de l'interpolée.
"""

from matplotlib import pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from pathlib import Path
import argparse
import json


def process_slurm_jobs(parentdir: str) -> tuple:
    """
    Parcourt les répertoires de calcul pour assembler les données.
    Retourne le tableau (T, <m>) et le dictionnaire descriptif d'un run
    """
    json_filename = "run.json"

    # Liste des répertoires de run
    jobdirs = [f for f in Path(parentdir).iterdir() if f.is_dir()]
    if len(jobdirs) == 0:
        exit(f"Aucun répertoire de job trouvé dans {parentdir}")
    data = []
    # On parcourt les répretoires de run
    for jobdir in jobdirs:
        try:
            # On lit le fichier json
            with open(jobdir / json_filename) as f:
                run = json.load(f)
                # On ajoute la valeur de la température et de la moyennisation
                # à la liste data
                data.extend([[int(T), res['m1_moyen']]
                            for T, res in run['resultats'].items()])
        except FileNotFoundError:
            print(f"Attention: fichier {json_filename} absent "
                  f"dans {jobdir.as_posix()}")

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def process_json(json_filepath: Path):
    """
    Retourne le tableau (T, <m>) à partir du fichier run.json
    et le dictionnaire descriptif du run
    """
    with open(json_filepath) as f:
        run = json.load(f)

    data = [[int(T), res['m1_moyen']] for T, res in run['resultats'].items()]

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def plot(data: np.array, parentdir: str, run: dict, show: bool, taille_section, loi_curie, T_Curie_tab):
    """
    Trace les données (T, <m>), interpole les valeurs,
    calcule la température de Curie.
    Exporte en PNG.
    """

    temperature, m1_moy = data[:, 0], data[:, 1]
    interp = interp1d(temperature, m1_moy, kind='cubic')

    
    params = run['params']

    
    
    
    plt.figure(1)

    if params['dx'] == 1.e-9:
            plt.title(fr"{params['element']}  ${params['Jx']}$nm $\times {params['Jy']}$nm $\times{params['Jz']}$nm")
            plt.plot(temperature[0:-1:2], m1_moy[0:-1:2], 'go', label='dx=1nm')
    if params['dx'] == 1.25e-9:
            plt.plot(temperature[0:-1:2], m1_moy[0:-1:2], 'r.', label='dx=1.25nm')
    if params['dx'] == 2.e-9:
            plt.plot(temperature[0:-1:2], m1_moy[0:-1:2], 'b+', label='dx=2nm')
    if params['dx'] == 2.5e-9:
            plt.plot(temperature[0:-1:2], m1_moy[0:-1:2], 'kx', label='dx=2.5nm')
    if params['dx'] == 5.e-9:
            plt.plot(temperature[0:-1:2], m1_moy[0:-1:2], 'y.', linewidth= 0.5, label='dx=5nm')
    
    
    T = np.linspace(temperature.min(), temperature.max(), 20000)

    i_max = np.where(0.1-interp(T)>0)[0].min()
    T_Curie = T[i_max]
    print(f"{T_Curie = :.08f} K")
    
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.xlabel('Temperature [K]', fontsize = 15)
    plt.ylabel(r'Magnetization $M_{\mathrm{tot}}$', fontsize = 15)
    # cobalt
    #plt.axis([1300, 1500, -0.05, 0.45]) 
    # fer
    #plt.axis([1000, 1200, -0.05, 0.45])
    # nickel
    #plt.axis([450, 700, -0.05, 0.45])
    plt.plot([-50, 1400], [0, 0], 'k')
    
    # cobalt
    #plt.plot([1388, 1388], [-0.1, 1.1], 'k-.')
    # fer
    #plt.plot([1043, 1043], [-0.1, 1.1], 'k-.')
    # nickel
    plt.plot([627, 627], [-0.1, 1.1], 'k-.')

    plt.axis([0, 1400, -0.1, 1.1])
    plt.grid(True, which="both")
    plt.legend(loc='best', fontsize = 15)
    
    

    

    taille_section.append(params['Jz'])
    T_Curie_tab.append(T_Curie)

    

    if show:
        plt.show()

    


def main():
    """
    Interprète la ligne de commande pour exécuter les fonctions de traitement
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--job_dir1', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir2', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir3', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir4', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir5', type=str, help='Slurm main job directory')
    parser.add_argument('--json_file', type=str, default='run.json',
                        help='Chemin du fichier run.json')
    parser.add_argument('-s', '--show', action='store_true', default=False,
                        help="Affiche le graphe dans une fenêtre graphique")
    args = parser.parse_args()
    if args.job_dir1:
        data, run = process_slurm_jobs(args.job_dir1)
        fig1 = plt.figure(1)
        plt.clf()
        
        taille_section = []
        loi_curie = []
        T_Curie_tab=[]
        plot(data, args.job_dir1, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir2:
        data, run = process_slurm_jobs(args.job_dir2)
        plot(data, args.job_dir2, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir3:
        data, run = process_slurm_jobs(args.job_dir3)
        plot(data, args.job_dir3, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir4:
        data, run = process_slurm_jobs(args.job_dir4)
        plot(data, args.job_dir4, run, args.show, taille_section, loi_curie, T_Curie_tab)

    
    if args.job_dir5:
        data, run = process_slurm_jobs(args.job_dir5)
        plot(data, args.job_dir5, run, args.show, taille_section, loi_curie, T_Curie_tab)
        fig1.tight_layout()
        image_filename = Path(args.job_dir5) / 'nickel_differents_dx_meme_geometrie_50_50_50.png'
        fig1.savefig(image_filename)
        print(f"Image enregistrée dans {image_filename}")

   


    else:
        data, run = process_json(Path(args.json_file))
        plot(data, Path(args.json_file).parent, run, args.show)


if __name__ == '__main__':
    main()
