#!/usr/bin/env python3
"""
NOM DU FICHIER : process_temperature_regression2.py
Post-traite un ensemble de runs regroupés en un fichier run.json ou
en un ensemble de job arrays SLURM :
  - Extrait les données de résultats,
  - Trace la magnétisation moyenne calculée en fonction de la température,
  - Interpole les points calculés sur des splines cubiques,
  - Détermine la température de Curie comme la valeur correspondant à
    la pente (négative) minimale de l'interpolée.
"""

from matplotlib import pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from pathlib import Path
import argparse
import json


def process_slurm_jobs(parentdir: str) -> tuple:
    """
    Parcourt les répertoires de calcul pour assembler les données.
    Retourne le tableau (T, <m>) et le dictionnaire descriptif d'un run
    """
    json_filename = "run.json"

    # Liste des répertoires de run
    jobdirs = [f for f in Path(parentdir).iterdir() if f.is_dir()]
    if len(jobdirs) == 0:
        exit(f"Aucun répertoire de job trouvé dans {parentdir}")
    data = []
    # On parcourt les répretoires de run
    for jobdir in jobdirs:
        try:
            # On lit le fichier json
            with open(jobdir / json_filename) as f:
                run = json.load(f)
                # On ajoute la valeur de la température et de la moyennisation
                # à la liste data
                data.extend([[int(T), res['m1_moyen']]
                            for T, res in run['resultats'].items()])
        except FileNotFoundError:
            print(f"Attention: fichier {json_filename} absent "
                  f"dans {jobdir.as_posix()}")

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def process_json(json_filepath: Path):
    """
    Retourne le tableau (T, <m>) à partir du fichier run.json
    et le dictionnaire descriptif du run
    """
    with open(json_filepath) as f:
        run = json.load(f)

    data = [[int(T), res['m1_moyen']] for T, res in run['resultats'].items()]

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def plot(data: np.array, parentdir: str, run: dict, show: bool, taille_section, loi_curie, T_Curie_tab):
    """
    Trace les données (T, <m>), interpole les valeurs,
    calcule la température de Curie.
    Exporte en PNG.
    """

    temperature, m1_moy = data[:, 0], data[:, 1]
    interp = interp1d(temperature, m1_moy, kind='cubic')

    
    params = run['params']

    plt.figure(5)
    plt.title(params['element'] +
              fr" (taille en $x$ : {params['Jx']}, $dx = ${params['dx']})",
              fontdict={'size': 10})

    plt.figure(4)
    plt.title(params['element'] +
              fr" (taille en $x$ : {params['Jx']}, $dx = ${params['dx']})",
              fontdict={'size': 10})
    

    plt.figure(3)
    plt.title(params['element'] +
              fr" (taille en $x$ : {params['Jx']}, $dx = ${params['dx']})",
              fontdict={'size': 10})
    

    plt.figure(2)
    if params['element'] == 'Fer':
        plt.title('Iron' +
              fr" nanolayer",
              fontdict={'size': 15})
    else:
        plt.title(params['element'] +
              fr" nanolayer",
              fontdict={'size': 15})
    
    
    plt.figure(1)
    plt.title(params['element'] +
              fr" ($dx = ${params['dx']})",
              fontdict={'size': 10})
    plt.plot(temperature, m1_moy, 'o')

    T = np.linspace(temperature.min(), temperature.max(), 20000)

    i_max = np.where(0.1-interp(T)>0)[0].min()
    T_Curie = T[i_max]
    print(f"{T_Curie = :.08f} K")
    plt.plot(T, interp(T), label=fr"${params['Jy']}\times{params['Jz']}$, {T_Curie = :.0f}")#, fontsize = 15
    plt.xlabel('Température [K]')
    plt.ylabel('Magnétisation')
    # cobalt
    #plt.axis([1300, 1500, -0.05, 0.45]) 
    # fer
    #plt.axis([1000, 1200, -0.05, 0.45])
    # nickel
    #plt.axis([450, 700, -0.05, 0.45])
    plt.plot(T, np.zeros(np.shape(T)), 'k')
    plt.grid('on')
    #plt.legend(loc='best')

    plt.figure(4)
    plt.loglog(1.-T[i_max-700:i_max-100]/T_Curie, interp(T)[i_max-700:i_max-100], '-')
    

    plt.figure(5)
    plt.loglog(T[50:200]/T_Curie, 1.-interp(T)[50:200], '-')
    
    taille_section.append(params['Jz'])
    T_Curie_tab.append(T_Curie)
   
    

    

    if show:
        plt.show()

    


def main():
    """
    Interprète la ligne de commande pour exécuter les fonctions de traitement
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--job_dir1', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir2', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir3', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir4', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir5', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir6', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir7', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir8', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir9', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir10', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir11', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir12', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir13', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir14', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir15', type=str, help='Slurm main job directory')
    parser.add_argument('--json_file', type=str, default='run.json',
                        help='Chemin du fichier run.json')
    parser.add_argument('-s', '--show', action='store_true', default=False,
                        help="Affiche le graphe dans une fenêtre graphique")
    args = parser.parse_args()
    if args.job_dir1:
        data, run = process_slurm_jobs(args.job_dir1)
        fig1 = plt.figure(1)
        plt.clf()
        fig1.suptitle("Magnétisation moyenne en fonction de la température")

        fig2 = plt.figure(2)
        plt.clf()
        #fig2.suptitle("Loi de Curie en fonction du côté de la section carrée")

        fig3 = plt.figure(3)
        plt.clf()
        fig3.suptitle("Temperature de Curie en fonction du côté de la section carrée")

        fig4 = plt.figure(4)
        plt.clf()
        fig4.suptitle("Loi proche de la temperature de Curie")

        fig5 = plt.figure(5)
        plt.clf()
        fig5.suptitle("Loi de Bloch")

        taille_section = []
        loi_curie = []
        T_Curie_tab=[]
        
        plot(data, args.job_dir1, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir2:
        data, run = process_slurm_jobs(args.job_dir2)
        plot(data, args.job_dir2, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir3:
        data, run = process_slurm_jobs(args.job_dir3)
        plot(data, args.job_dir3, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir4:
        data, run = process_slurm_jobs(args.job_dir4)
        plot(data, args.job_dir4, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir5:
        data, run = process_slurm_jobs(args.job_dir5)
        plot(data, args.job_dir5, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir6:
        data, run = process_slurm_jobs(args.job_dir6)
        plot(data, args.job_dir6, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir7:
        data, run = process_slurm_jobs(args.job_dir7)
        plot(data, args.job_dir7, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir8:
        data, run = process_slurm_jobs(args.job_dir8)
        plot(data, args.job_dir8, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir9:
        data, run = process_slurm_jobs(args.job_dir9)
        plot(data, args.job_dir9, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir10:
        data, run = process_slurm_jobs(args.job_dir10)
        plot(data, args.job_dir10, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir11:
        data, run = process_slurm_jobs(args.job_dir11)
        plot(data, args.job_dir11, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir12:
        data, run = process_slurm_jobs(args.job_dir12)
        plot(data, args.job_dir12, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir13:
        data, run = process_slurm_jobs(args.job_dir13)
        plot(data, args.job_dir13, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir14:
        data, run = process_slurm_jobs(args.job_dir14)
        plot(data, args.job_dir14, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir15:
        data, run = process_slurm_jobs(args.job_dir15)
        plot(data, args.job_dir15, run, args.show, taille_section, loi_curie, T_Curie_tab)
        image_filename = Path(args.job_dir15) / 'm1_moy2.png'
        fig1.savefig(image_filename)
        print(f"Image enregistrée dans {image_filename}")

        plt.figure(2)
        T_Curie_inf=T_Curie_tab[np.argmax(taille_section)]
        loi_curie=(T_Curie_inf-T_Curie_tab)/T_Curie_inf
        indices=np.nonzero(loi_curie)[0]
        taille_section_plot=[]
        loi_curie_plot=[]
        for i in indices:
            taille_section_plot.append(taille_section[i])
            loi_curie_plot.append(loi_curie[i])
        taille_section_plot=np.array(taille_section_plot)
        loi_curie_plot=np.array(loi_curie_plot)

   
        plt.loglog(taille_section_plot, loi_curie_plot, 'o')
        #P = np.polyfit(np.log(taille_section[I_index[np.min(indices):]]), np.log(loi_curie[I_index[np.min(indices):]]), 1)
        P = np.polyfit(np.log(taille_section_plot[3:]), np.log(loi_curie_plot[3:]), 1)
        plt.loglog(np.linspace(10, 45, 101), np.exp(P[1])*(np.linspace(10, 45, 101))**P[0], "r", label=fr'$({(np.exp(P[1]))**(-1./P[0]) :.2f}/d)^{{{-P[0] :0.2f}}}$')

        plt.xticks(fontsize=15)
        plt.yticks(fontsize=15)
    
        plt.xlabel('Thickness of side $d$ [nm]', fontsize = 15)
        plt.ylabel('Reduced temperature $(T_C(\infty)-T_C(d))/T_C(\infty)$     ', fontsize = 15)
        plt.grid(True, which="both")

        
        
        plt.legend(loc='best', fontsize = 15)
        fig2.tight_layout()
        image_filename = Path(args.job_dir15) / 'm1_loi_curie_article.png'
        fig2.savefig(image_filename)
        print(f"Image enregistrée dans {image_filename}")

        plt.figure(4)
        plt.loglog(np.linspace(7.e-3, 7.e-2, 200), 1*np.sqrt(np.linspace(7.e-3, 7.e-2, 200)), 'r', label=fr'$1/2$ slope')
        plt.legend(loc='best')
        plt.xlabel('$1-T/T_C$')
        plt.ylabel('$m$')
        plt.grid('on')
        #image_filename = Path(args.job_dir15) / 'loi_proche_curie.png'
        #fig4.savefig(image_filename)
        #print(f"Image enregistrée dans {image_filename}")

        plt.figure(5)
        plt.loglog(np.linspace(3.e-3, 3.e-2, 20), (np.linspace(3.e-3, 3.e-2, 20))**(3./2), 'r', label=fr'$3/2$ slope')
        plt.legend(loc='best')
        plt.xlabel('$T/T_C$')
        plt.ylabel('$1-m$')
        plt.grid('on')
        #image_filename = Path(args.job_dir15) / 'loi_bloch.png'
        #fig5.savefig(image_filename)
        #print(f"Image enregistrée dans {image_filename}")

    else:
        data, run = process_json(Path(args.json_file))
        plot(data, Path(args.json_file).parent, run, args.show)


if __name__ == '__main__':
    main()
