#!/usr/bin/env python3
"""
Post-traite un ensemble de runs regroupés en un fichier run.json ou
en un ensemble de job arrays SLURM :
  - Extrait les données de résultats,
  - Trace la magnétisation moyenne calculée en fonction de la température,
  - Interpole les points calculés sur des splines cubiques,
  - Détermine la température de Curie comme la valeur correspondant à
    la pente (négative) minimale de l'interpolée.
"""

from matplotlib import pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from pathlib import Path
import argparse
import json


def process_slurm_jobs(parentdir: str) -> tuple:
    """
    Parcourt les répertoires de calcul pour assembler les données.
    Retourne le tableau (T, <m>) et le dictionnaire descriptif d'un run
    """
    json_filename = "run.json"

    # Liste des répertoires de run
    jobdirs = [f for f in Path(parentdir).iterdir() if f.is_dir()]
    if len(jobdirs) == 0:
        exit(f"Aucun répertoire de job trouvé dans {parentdir}")
    data = []
    # On parcourt les répretoires de run
    for jobdir in jobdirs:
        try:
            # On lit le fichier json
            with open(jobdir / json_filename) as f:
                run = json.load(f)
                # On ajoute la valeur de la température et de la moyennisation
                # à la liste data
                data.extend([[int(T), res['m1_moyen']]
                            for T, res in run['resultats'].items()])
        except FileNotFoundError:
            print(f"Attention: fichier {json_filename} absent "
                  f"dans {jobdir.as_posix()}")

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def process_json(json_filepath: Path):
    """
    Retourne le tableau (T, <m>) à partir du fichier run.json
    et le dictionnaire descriptif du run
    """
    with open(json_filepath) as f:
        run = json.load(f)

    data = [[int(T), res['m1_moyen']] for T, res in run['resultats'].items()]

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def plot(data: np.array, parentdir: str, run: dict, show: bool):
    """
    Trace les données (T, <m>), interpole les valeurs,
    calcule la température de Curie.
    Exporte en PNG.
    """

    temperature, m1_moy = data[:, 0], data[:, 1]
    interp = interp1d(temperature, m1_moy, kind='cubic')

    fig2 = plt.figure(2)
    fig2.suptitle("Dérivée de la magnétisation moyenne en fonction de la température")
    params = run['params']
    plt.title(params['element'] +
              fr", ${params['Jx']}$"
              fr" ($dx = ${params['dx']})",
              fontdict={'size': 10})
    


    fig1 = plt.figure(1)
    fig1.suptitle("Magnétisation moyenne en fonction de la température")
    params = run['params']
    plt.title(params['element'] +
              fr", ${params['Jx']}\times{params['Jy']}\times{params['Jz']}$"
              fr" ($dx = ${params['dx']})",
              fontdict={'size': 10})
    plt.plot(temperature, m1_moy, 'o', label="calculée")

    T = np.linspace(temperature.min(), temperature.max(), 200)

    plt.plot(T, interp(T), label="interpolée (cubique)")

    derivee=(m1_moy[3:-1:3]-m1_moy[0:-4:3])/(temperature[3:-1:3]-temperature[0:-4:3])
    #interp3=interp1d(temperature[0:-4:3], -derivee/(-derivee).max(), kind='cubic')
    #T3 = np.linspace(temperature[0:-4:3].min(), temperature[0:-4:3].max(), 20000)

    #i_max = np.where(0.05-interp(T)>0)[0].min()
    i_max=np.argmax(-derivee)
    T_Curie = temperature[i_max*3]
    #i_max = np.gradient(interp(T), T).argmin()
    #i_max = np.where(0.1-interp(T)>0)[0].min()
    #T_Curie = T[i_max]
    print(f"{T_Curie = :.0f} K")
    plt.annotate("$T_{{Curie}} = {:.0f} K$".format(T_Curie),
                 xy=(T_Curie, interp(T_Curie)),
                 xytext=(T_Curie + 20, interp(T_Curie) + 0.01))
    plt.axvline(x=T_Curie, color='k')
    plt.xlabel('Température [K]')
    plt.ylabel('Magnétisation')
    plt.plot(T, np.zeros(np.shape(T)), 'k')
    plt.grid('on')
    plt.legend()

    plt.figure(2)
    plt.plot(temperature[0:-4:3], -derivee/(-derivee).max(), label=fr" (${params['Jy']}\times{params['Jz']}$)")
    #plt.plot(T3, interp3(T3), label="calculée")
    #print((-interp3(T3)).max(), i_max)
    #cobalt
    #plt.axis([1250, 1500, -0.2, 1.])
    #fer
    #plt.axis([900, 1150, -0.2, 1.])
    #nickel
    plt.axis([500, 600, -0.2, 1.])
    plt.legend(loc='best')
    plt.grid('on')
    plt.xlabel('Température [K]')
    plt.ylabel('-dM/dT normalisé')


    if show:
        plt.show()

    


def main():
    """
    Interprète la ligne de commande pour exécuter les fonctions de traitement
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--job_dir1', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir2', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir3', type=str, help='Slurm main job directory')
    parser.add_argument('--json_file', type=str, default='run.json',
                        help='Chemin du fichier run.json')
    parser.add_argument('-s', '--show', action='store_true', default=False,
                        help="Affiche le graphe dans une fenêtre graphique")
    args = parser.parse_args()
    fig1=plt.figure(1)
    plt.clf()
    fig2=plt.figure(2)
    plt.clf()
    if args.job_dir1:
        data, run = process_slurm_jobs(args.job_dir1)
        plot(data, args.job_dir1, run, args.show)

    if args.job_dir2:
        data, run = process_slurm_jobs(args.job_dir2)
        plot(data, args.job_dir2, run, args.show)
    
    if args.job_dir3:
        data, run = process_slurm_jobs(args.job_dir3)
        plot(data, args.job_dir3, run, args.show)

        image_filename = Path(args.job_dir3) / 'm1_moy.png'
        fig1.savefig(image_filename)
        print(f"Image enregistrée dans {image_filename}")


        image_filename = Path(args.job_dir3) / 'derivee.png'
        fig2.savefig(image_filename)
        print(f"Image enregistrée dans {image_filename}")

    else:
        data, run = process_json(Path(args.json_file))
        plot(data, Path(args.json_file).parent, run, args.show)

    


if __name__ == '__main__':
    main()
