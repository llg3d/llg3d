#!/usr/bin/env python3
"""
Post-traite un ensemble de runs regroupés en un fichier run.json ou
en un ensemble de job arrays SLURM :
  - Extrait les données de résultats,
  - Trace la magnétisation moyenne calculée en fonction de la température,
  - Interpole les points calculés sur des splines cubiques,
  - Détermine la température de Curie comme la valeur correspondant à
    la pente (négative) minimale de l'interpolée.
"""

from matplotlib import pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from pathlib import Path
import argparse
import json


def process_slurm_jobs(parentdir: str) -> tuple:
    """
    Parcourt les répertoires de calcul pour assembler les données.
    Retourne le tableau (T, <m>) et le dictionnaire descriptif d'un run
    """
    json_filename = "run.json"

    # Liste des répertoires de run
    jobdirs = [f for f in Path(parentdir).iterdir() if f.is_dir()]
    if len(jobdirs) == 0:
        exit(f"Aucun répertoire de job trouvé dans {parentdir}")
    data = []
    # On parcourt les répretoires de run
    for jobdir in jobdirs:
        try:
            # On lit le fichier json
            with open(jobdir / json_filename) as f:
                run = json.load(f)
                # On ajoute la valeur de la température et de la moyennisation
                # à la liste data
                data.extend([[int(T), res['m1_moyen']]
                            for T, res in run['resultats'].items()])
        except FileNotFoundError:
            print(f"Attention: fichier {json_filename} absent "
                  f"dans {jobdir.as_posix()}")

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def process_json(json_filepath: Path):
    """
    Retourne le tableau (T, <m>) à partir du fichier run.json
    et le dictionnaire descriptif du run
    """
    with open(json_filepath) as f:
        run = json.load(f)

    data = [[int(T), res['m1_moyen']] for T, res in run['resultats'].items()]

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def plot(data: np.array, parentdir: str, run: dict, show: bool):
    """
    Trace les données (T, <m>), interpole les valeurs,
    calcule la température de Curie.
    Exporte en PNG.
    """

    temperature, m1_moy = data[:, 0], data[:, 1]
    interp = interp1d(temperature, m1_moy, kind='cubic')

    fig = plt.figure()
    fig.suptitle("Magnétisation moyenne en fonction de la température")
    params = run['params']
    plt.title(params['element'] +
              fr", ${params['Jx']}\times{params['Jy']}\times{params['Jz']}$"
              fr" ($dx = ${params['dx']})",
              fontdict={'size': 10})
    plt.plot(temperature, m1_moy, 'o', label="calculée")

    T = np.linspace(temperature.min(), temperature.max(), 200)

    plt.plot(T, interp(T), label="interpolée (cubique)")

    #i_max = np.gradient(interp(T), T).argmin()
    i_max = np.where(0.1-interp(T)>0)[0].min()
    T_Curie = T[i_max]
    print(f"{T_Curie = :.0f} K")
    plt.annotate("$T_{{Curie}} = {:.0f} K$".format(T_Curie),
                 xy=(T_Curie, interp(T_Curie)),
                 xytext=(T_Curie + 20, interp(T_Curie) + 0.01))
    plt.axvline(x=T_Curie, color='k')
    plt.xlabel('Température [K]')
    plt.ylabel('Magnétisation')
    plt.plot(T, np.zeros(np.shape(T)), 'k')
    plt.grid('on')
    plt.legend()

    if show:
        plt.show()

    image_filename = Path(parentdir) / 'm1_moy.png'
    fig.savefig(image_filename)
    print(f"Image enregistrée dans {image_filename}")


def main():
    """
    Interprète la ligne de commande pour exécuter les fonctions de traitement
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--job_dir', type=str, help='Slurm main job directory')
    parser.add_argument('--json_file', type=str, default='run.json',
                        help='Chemin du fichier run.json')
    parser.add_argument('-s', '--show', action='store_true', default=False,
                        help="Affiche le graphe dans une fenêtre graphique")
    args = parser.parse_args()
    if args.job_dir:
        data, run = process_slurm_jobs(args.job_dir)
        plot(data, args.job_dir, run, args.show)
    else:
        data, run = process_json(Path(args.json_file))
        plot(data, Path(args.json_file).parent, run, args.show)


if __name__ == '__main__':
    main()
