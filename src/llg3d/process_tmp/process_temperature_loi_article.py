#!/usr/bin/env python3
"""
NOM : process_temperature_loi_article.py
Post-traite un ensemble de runs regroupés en un fichier run.json ou
en un ensemble de job arrays SLURM :
  - Extrait les données de résultats,
  - Trace la magnétisation moyenne calculée en fonction de la température,
  - Interpole les points calculés sur des splines cubiques,
  - Détermine la température de Curie comme la valeur correspondant à
    la pente (négative) minimale de l'interpolée.
"""

from matplotlib import pyplot as plt
import numpy as np
from scipy.interpolate import interp1d
from pathlib import Path
import argparse
import json


def process_slurm_jobs(parentdir: str) -> tuple:
    """
    Parcourt les répertoires de calcul pour assembler les données.
    Retourne le tableau (T, <m>) et le dictionnaire descriptif d'un run
    """
    json_filename = "run.json"

    # Liste des répertoires de run
    jobdirs = [f for f in Path(parentdir).iterdir() if f.is_dir()]
    if len(jobdirs) == 0:
        exit(f"Aucun répertoire de job trouvé dans {parentdir}")
    data = []
    # On parcourt les répretoires de run
    for jobdir in jobdirs:
        try:
            # On lit le fichier json
            with open(jobdir / json_filename) as f:
                run = json.load(f)
                # On ajoute la valeur de la température et de la moyennisation
                # à la liste data
                data.extend([[int(T), res['m1_moyen']]
                            for T, res in run['resultats'].items()])
        except FileNotFoundError:
            print(f"Attention: fichier {json_filename} absent "
                  f"dans {jobdir.as_posix()}")

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def process_json(json_filepath: Path):
    """
    Retourne le tableau (T, <m>) à partir du fichier run.json
    et le dictionnaire descriptif du run
    """
    with open(json_filepath) as f:
        run = json.load(f)

    data = [[int(T), res['m1_moyen']] for T, res in run['resultats'].items()]

    data.sort()  # On ordonne suivant les températures croissantes

    return np.array(data), run


def plot(data: np.array, parentdir: str, run: dict, show: bool, taille_section, loi_curie, T_Curie_tab):
    """
    Trace les données (T, <m>), interpole les valeurs,
    calcule la température de Curie.
    Exporte en PNG.
    """

    temperature, m1_moy = data[:, 0], data[:, 1]
    interp = interp1d(temperature, m1_moy, kind='cubic')

    
    params = run['params']

    plt.figure(5)
    plt.title(
              fr" nanolayer",
              fontdict={'size': 15})

    plt.figure(4)
    plt.title(
              fr" nanolayer",
              fontdict={'size': 15})
    

    T = np.linspace(temperature.min(), temperature.max(), 20000)

    i_max = np.where(0.1-interp(T)>0)[0].min()
    T_Curie = T[i_max]
    print(f"{T_Curie = :.08f} K")



    plt.figure(4)
    plt.loglog(1, 1, 'b') # pour la legende
    plt.loglog(1, 1, 'r') # pour la legende
    plt.loglog(1, 1, 'g') # pour la legende
    plt.loglog(1, 1, 'k.') # pour la legende
    plt.loglog(1, 1, 'k+') # pour la legende
    plt.loglog(1, 1, 'k') # pour la legende

    if params['Jz']==11 and params['element']=='Cobalt':
        plt.loglog(1.-T[i_max-700:i_max-100:15]/T_Curie, interp(T)[i_max-700:i_max-100:15], 'b+')

    if params['Jz']==41 and params['element']=='Cobalt':
        plt.loglog(1.-T[i_max-700:i_max-100:15]/T_Curie, interp(T)[i_max-700:i_max-100:15], 'b.')

    if params['Jz']==11 and params['element']=='Fer':
        plt.loglog(1.-T[i_max-700:i_max-100:15]/T_Curie, 1.5*interp(T)[i_max-700:i_max-100:15], 'r+')

    if params['Jz']==41 and params['element']=='Fer':
        plt.loglog(1.-T[i_max-700:i_max-100:15]/T_Curie, 1.5*interp(T)[i_max-700:i_max-100:15], 'r.')

    if params['Jz']==11 and params['element']=='Nickel':
        plt.loglog(1.-T[i_max-700:i_max-100:15]/T_Curie, 0.5*interp(T)[i_max-700:i_max-100:15], 'g+')

    if params['Jz']==41 and params['element']=='Nickel':
        plt.loglog(1.-T[i_max-700:i_max-100:15]/T_Curie, 0.5*interp(T)[i_max-700:i_max-100:15], 'g.')
    

    plt.figure(5)
    plt.loglog(1, 1, 'b') # pour la legende
    plt.loglog(1, 1, 'r') # pour la legende
    plt.loglog(1, 1, 'g') # pour la legende
    plt.loglog(1, 1, 'k.') # pour la legende
    plt.loglog(1, 1, 'k+') # pour la legende
    plt.loglog(1, 1, 'k') # pour la legende

    if params['Jz']==11 and params['element']=='Cobalt':
        plt.loglog(T[50:200:10]/T_Curie, 1.-interp(T)[50:200:10], 'b+')
    if params['Jz']==41 and params['element']=='Cobalt':
        plt.loglog(T[50:200:10]/T_Curie, 1.-interp(T)[50:200:10], 'b.')
    if params['Jz']==11 and params['element']=='Fer':
        plt.loglog(T[50:200:10]/T_Curie, 1.5*(1.-interp(T)[50:200:10]), 'r+')
    if params['Jz']==41 and params['element']=='Fer':
        plt.loglog(T[50:200:10]/T_Curie, 1.5*(1.-interp(T)[50:200:10]), 'r.')
    if params['Jz']==11 and params['element']=='Nickel':
        plt.loglog(T[50:200:10]/T_Curie, 0.5*(1.-interp(T)[50:200:10]), 'g+')
    if params['Jz']==41 and params['element']=='Nickel':
        plt.loglog(T[50:200:10]/T_Curie, 0.5*(1.-interp(T)[50:200:10]), 'g.')


    taille_section.append(params['Jz'])
    T_Curie_tab.append(T_Curie)
   
    

    

    if show:
        plt.show()

    


def main():
    """
    Interprète la ligne de commande pour exécuter les fonctions de traitement
    """
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--job_dir1', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir2', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir3', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir4', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir5', type=str, help='Slurm main job directory')
    parser.add_argument('--job_dir6', type=str, help='Slurm main job directory')
    parser.add_argument('--json_file', type=str, default='run.json',
                        help='Chemin du fichier run.json')
    parser.add_argument('-s', '--show', action='store_true', default=False,
                        help="Affiche le graphe dans une fenêtre graphique")
    args = parser.parse_args()
    if args.job_dir1:
        data, run = process_slurm_jobs(args.job_dir1)
        fig1 = plt.figure(1)
        plt.clf()
        
        
        fig4 = plt.figure(4)
        plt.clf()
        

        fig5 = plt.figure(5)
        plt.clf()
        
        

        taille_section = []
        loi_curie = []
        T_Curie_tab=[]
        
        plot(data, args.job_dir1, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir2:
        data, run = process_slurm_jobs(args.job_dir2)
        plot(data, args.job_dir2, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir3:
        data, run = process_slurm_jobs(args.job_dir3)
        plot(data, args.job_dir3, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir4:
        data, run = process_slurm_jobs(args.job_dir4)
        plot(data, args.job_dir4, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir5:
        data, run = process_slurm_jobs(args.job_dir5)
        plot(data, args.job_dir5, run, args.show, taille_section, loi_curie, T_Curie_tab)
    if args.job_dir6:
        data, run = process_slurm_jobs(args.job_dir6)

        plt.figure(4)
        plt.loglog(np.linspace(7.e-3, 7.e-2, 200), 1*np.sqrt(np.linspace(7.e-3, 7.e-2, 200)), 'k')
        plt.legend(['Cobalt', 'Iron', 'Nickel', '$d=41$', '$d=11$', '$1/2$ slope'], loc='best', fontsize = 15, ncol=3)
        plt.xticks(fontsize=15)
        plt.yticks(fontsize=15)
        plt.xlabel('$1-T/T_C$', fontsize = 15)
        plt.ylabel('Magnetization $M_{\mathrm{tot}}$', fontsize = 15)
        plt.axis([6.0e-3, 8.1e-2, 7.5e-2, 8.1e-1])
        plt.grid(True, which = 'both')
        fig4.tight_layout()
        image_filename = Path(args.job_dir6) / 'loi_proche_curie_article2.png'
        fig4.savefig(image_filename)
        print(f"Image enregistrée dans {image_filename}")

        plt.figure(5)
        plt.loglog(np.linspace(3.e-3, 3.e-2, 20), (np.linspace(3.e-3, 3.e-2, 20))**(3./2), 'k')
        plt.legend(['Cobalt', 'Iron', 'Nickel', '$d=41$', '$d=11$', '$3/2$ slope'], loc='best', fontsize = 15)
        plt.xticks(fontsize=15)
        plt.yticks(fontsize=15)
        plt.xlabel('$T/T_C$', fontsize = 15)
        plt.ylabel('$(1-M_{\mathrm{tot}}$)', fontsize = 15)
        plt.axis([2.5e-3, 3.2e-2, 9.9e-5, 9.e-3])
        plt.grid(True, which = 'both')
        fig5.tight_layout()
        image_filename = Path(args.job_dir6) / 'loi_bloch_article2.png'
        fig5.savefig(image_filename)
        print(f"Image enregistrée dans {image_filename}")
        

    else:
        data, run = process_json(Path(args.json_file))
        plot(data, Path(args.json_file).parent, run, args.show)


if __name__ == '__main__':
    main()
